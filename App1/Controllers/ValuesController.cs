﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App1.Models;
using Microsoft.AspNetCore.Mvc;

namespace App1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new String[] {"Done"};
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            TestDBContext testDBContext = new TestDBContext();
            var userAccounts = new UserAccounts();
            var context = testDBContext.UserAccounts.ToList();
            return new OkObjectResult(testDBContext.UserAccounts.Where(x => x.Id == id).Single());
        }

        // POST api/values
        [HttpPost]
        public void Post(UserAccounts userAccounts)
        {
            if(userAccounts == null) {
            }
                
            TestDBContext testDBContext = new TestDBContext();
            testDBContext.UserAccounts.Add(userAccounts);
            testDBContext.SaveChangesAsync();
            // return testDBContext.UserAccounts.Find(userAccounts.Id).Name;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
