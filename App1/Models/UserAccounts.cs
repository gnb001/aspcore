﻿using System;
using System.Collections.Generic;

namespace App1.Models
{
    public partial class UserAccounts
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Age { get; set; }
        public DateTime? Bday { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public double? Salary { get; set; }
    }
}
